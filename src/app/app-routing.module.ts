import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from '../app/core/main/main.component';
import { ErrorComponent } from './core/error/error.component';
import { AuthGuard } from './core/_guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },

  {
    path: 'auth',
    loadChildren: () =>
      import('./pages/auth/auth.module').then((m) => m.AuthModule)
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginModule)
  },

  {
    path: 'register',
    loadChildren: () =>
      import('./pages/register/register.module').then((m) => m.RegisterModule)
  },





  {
    path: 'Manage',
    component: MainComponent,
    loadChildren: () =>
      import('./pages/manage/manage.module').then((m) => m.ManageModule)
  },





  {
    path: 'error',
    component: ErrorComponent,
    loadChildren: () =>
      import('./core/error/error.module').then((m) => m.ErrorModule)
  },
  {
    path: 'user_management',
    component: MainComponent,
    loadChildren: () =>
      import('./pages/user/user.module').then((m) => m.UserModule)
  },




  // {
  //   path: 'user_management',
  //   canActivate: [AuthGuard],
  //   data: { roles: ['admin', 'superadmin'] },
  //   component: MainComponent,
  //   loadChildren: () =>
  //     import('./features/users/users.module').then((m) => m.UsersModule)
  // },

  {
    path: '**',
    redirectTo: '/error'
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
