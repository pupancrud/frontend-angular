import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/shared/service/auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(private auth: AuthService, private router: Router) {}
  private checkPermission = (next: ActivatedRouteSnapshot): boolean => {
    let roles = [];
    roles = next.data.roles as Array<string>;
    let position = [];
    position = next.data.positions as Array<string>;
    if (roles) {
      return this.auth.isAccessPosition(position);
    } else {
      return false;
    }
  };
  private checkAuthorized = async (
    url: string,
    next: ActivatedRouteSnapshot
  ): Promise<boolean> => {
    return this.auth.isAuthorizedAccess().then((auth) => {
      if (!auth) {
        console.log('loop guard if')
        localStorage.clear();
        void this.router.navigateByUrl('/auth');
        return false;
      } else {
        void this.router.navigateByUrl('/Manage');

        if (this.checkPermission(next)) {
        console.log('loop guard else if')

          return true;
        } else {
        console.log('loop guard else')

          // void this.router.navigateByUrl('/error');
          return false;
        }
      }
    });
  };
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const url: string = state.url;
    return this.checkAuthorized(url, next);
  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.canActivate(next, state);
  }
}
