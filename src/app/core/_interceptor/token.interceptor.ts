import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';

export const InterceptorMatchList = [
  // new RegExp(`(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?/api/*`),
  new RegExp(`.*`),
];

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private router: Router, private message: NzMessageService) {}
  private match = (url: string): boolean => {
    for (const pattern of InterceptorMatchList) {
      // eslint-disable-next-line @typescript-eslint/prefer-regexp-exec
      if (Array.isArray(url.match(pattern))) {
        return true;
      }
    }
    return false;
  };

  intercept = (
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> => {
    const token = localStorage.getItem('token');

    if (token) {
      const cloned = request.clone({
        // headers: request.headers.set('Authorization', 'Bearer'+ token),
        headers:  request.headers.set(  "Authorization", "Bearer " + token ),
      });



      return next.handle(cloned);
    } else {
      return next.handle(request).pipe(
        catchError((err) => {
          if (err instanceof HttpErrorResponse && err.status === 401) {
            localStorage.clear();
            this.message.error(
              'คุณยังไม่ได้เข้าสู่ระบบ กรุณาลองใหม่อีกครั้งค่ะ',
              { nzDuration: 5 }
            );
            void this.router.navigateByUrl('/login');
          }
          // } else if (err instanceof HttpErrorResponse && err.status === 500) {
          //     this.router.navigateByUrl('/error', { state: Err500 });
          // }
          return throwError(err);
        })
      );

      // return next.handle(request);
    }

    // if (this.match(request.url)) {
    //   /** Attach token even if token is available */
    //   const token = localStorage.getItem("token");

    // }
    /** Return interceptor request */
  };
}

// ? comment

// return next.handle(request).pipe(
//   catchError((err) => {
//     if (err instanceof HttpErrorResponse && err.status === 401) {
//       localStorage.clear();
//       this.message.error(
//         'คุณยังไม่ได้เข้าสู่ระบบ กรุณาลองใหม่อีกครั้งค่ะ',
//         { nzDuration: 5 }
//       );
//       void this.router.navigateByUrl('/login');
//     }
//     // } else if (err instanceof HttpErrorResponse && err.status === 500) {
//     //     this.router.navigateByUrl('/error', { state: Err500 });
//     // }
//     return throwError(err);
//   })
// );
