import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/service/auth-service.service';
import {
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { ManageService } from 'src/app/shared/service/manage.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NzNotificationService } from 'ng-zorro-antd/notification';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {
  isCollapsed = false;
  name: string;

  constructor(
    private auth: AuthService,
    private router: Router,
    private manageApi: ManageService,
    private notification: NzNotificationService
  ) {
    this.name = '';
  }

  ngOnInit(): void {
    this.manageApi.getuserProfile().then(
      (res) => {
        localStorage.setItem('name', res.data.full_name);
        console.log(res.data.full_name);
        this.name = res.data.full_name;
      },
      (err: HttpErrorResponse) => {
        if (err.status === 504) {
         this.createNotification("error");
        void this.router.navigateByUrl('/login')
        } else {
        //  this.createNotification()
        this.createNotification("error");
        void this.router.navigateByUrl('/login')



        }
      }
    );
  }

  createNotification(type: string): void {
    this.notification.create(
      type,
      'ไม่สามารถติดต่อกับเซิฟเวอร์ได้',
      'กำลังออกจากระบบ....'
    );
  }

  signOut = (): void => {
    localStorage.clear();
    void this.router.navigate(['/login']);
  };
}
