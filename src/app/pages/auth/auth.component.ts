import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/service/auth-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  verify = false;
  constructor(
    private activatedRoute: ActivatedRoute,
    private auth: AuthService,
    private router: Router,
    private message: NzMessageService
  ) {}
  ngOnInit(): void {

    console.log('loop auth component')
    this.activatedRoute.queryParamMap.subscribe((p) => {
      const code = p.get('code');
      if (code && code !== '') {
        this.verify = true;
        this.auth.verifySSOAuthorizedCode(code).then(
          () => {
            this.verify = false;
            void this.router.navigateByUrl('/Manage');
          },
          (err: HttpErrorResponse) => {
            this.verify = false;
            console.log(err);
            if (err.status === 404) {
              this.message.error(
                'คุณไม่มีสิทธิ์ในการเข้าใช้งาน กรุณาติดต่อผู้ดูแลระบบ'
              );
            } else {
              this.message.error('ระบบมีปัญหากรุณาลองใหม่อีกครั้งค่ะ');
            }
            void this.router.navigateByUrl('/login');
          }
        );
      } else {
        this.auth.isAuthorizedAccess().then(
          (auth: boolean) => {
            if (auth) {

            console.log('pass login')
              void this.router.navigateByUrl('/Manage');
            } else {
            console.log('no login')


              void this.router.navigateByUrl('/login');
            }
          },
          (err) => {
            console.log('err');
            console.log('no login1')

            void this.router.navigateByUrl('/login');
          }
        );
      }
    });
  }
}
