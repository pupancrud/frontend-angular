import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { LoginService } from '../../shared/service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  validateForm!: FormGroup;
  isLoading: boolean;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private loginService: LoginService,
    private modalService: NzModalService
  ) {
    this.isLoading = false;
  }

  ngOnInit(): void {
    localStorage.clear();

    this.validateForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true],
    });
  }

  login = (): void => {
    var e = true;
    const data = {
      email: this.validateForm.controls.userName.value as string,
      password: this.validateForm.controls.password.value as string,
    };

    console.log(data);
    if (e === true) {
      this.isLoading = true;
      this.loginService.login(data).then(
        (res) => {
          if (res.message === 'Login Success') {
            console.log(res);
            var token = res.token;
            localStorage.setItem('token', token);

            void this.router.navigateByUrl('/Manage');
            this.isLoading = false;
          } else {
            this.isLoading = false;

            alert('ไม่สามารถเข้าสู่ระบบได้');
          }
        },
        (err: HttpErrorResponse) => {
          if (err.status === 401) {
            this.error(
              'ไม่สามารถเข้าสู่ระบบได้<br> เนื่องจากรหัสผ่านไม่ถูกต้อง'
            );
            this.isLoading = false;
          } else if (err.status === 404) {
            this.error('ไม่พบผู้ใช้งานนี้ในระบบ');
            this.isLoading = false;

          } else {
            this.error('ระบบมีปัญหากรุณาลองใหม่อีกครั้ง');
            this.isLoading = false;

          }
        }
      );
    }
  };
  submitForm = (): void => {
    for (const i in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(i)) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.validateForm.valid) {
      this.login();
    }
  };

  success = (msg: string): void => {
    const modal = this.modalService.success({
      nzTitle: 'Success',
      nzContent: `${msg}`,
      nzOkText: null,
    });
    setTimeout(() => modal.destroy(), 2000);
  };

  error = (msg: string): void => {
    const modal = this.modalService.error({
      nzTitle: 'Error',
      nzContent: `${msg}`,
    });
    setTimeout(() => modal.destroy(), 5000);
  };
}
