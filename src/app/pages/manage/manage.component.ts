import { Component, OnInit } from '@angular/core';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { Fruit } from '../_model/fruit-model';
// import { FruitserviceService } from '../service/fruitservice.service';
// import { NzModalService } from 'ng-zorro-antd/modal';

import { Datatable } from '../_model/manage-model';

// import { Observable, Observer } from 'rxjs';

// import { NzMessageService } from 'ng-zorro-antd/message';
// import { NzUploadFile } from 'ng-zorro-antd/upload';

import { LoginService } from '../../shared/service/login.service';
import { AuthService } from 'src/app/shared/service/auth-service.service';
import { ManageService } from 'src/app/shared/service/manage.service';

interface Person {
  key: string;
  name: string;
  age: number;
  address: string;
}

@Component({
  selector: 'app-car',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css'],
})
export class ManageComponent implements OnInit {
  // TODO     export มีไว้สำหรับ ประกาศตัวแปร
  str: string;

  constructor(
    private apicall: LoginService,
    private manageApi: ManageService,
    private auth: AuthService
  ) {
    // TODO  มีไว้สำหรับเรียกใช้ตัวแปร

    this.str = '';
  }

  ngOnInit() {

  }

  getApi = (): void => {
    this.auth.isAuthorizedAccess().then((res) => {
      console.log(res);
    });

    // this.apicall.getProfile().then((res) => {
    //   console.log(res);
    // });
  };

  // this.apicall.getApiTest().then((response) => {

  //   console.log(response);

  // })

  //   success = (msg: string): void => {
  //     const modal = this.modalService.success({
  //       nzTitle: 'Success',
  //       nzContent: `${msg}`,
  //       nzOkText: null,
  //     });
  //     setTimeout(() => modal.destroy(), 2000);
  //   };

  //   error = (msg: string): void => {
  //     const modal = this.modalService.error({
  //       nzTitle: 'Error',
  //       nzContent: `${msg}`,
  //     });
  //     setTimeout(() => modal.destroy(), 5000);
  //   };
  // }
}
