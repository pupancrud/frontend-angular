import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

import {
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';

import { Router } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Observable, Observer } from 'rxjs';
import { LoginService } from 'src/app/shared/service/login.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  registerForm!: FormGroup;
  isLoading: boolean;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private apicall: LoginService,
    private modalService: NzModalService
  ) {
    this.isLoading = false;

    this.registerForm = this.fb.group({
      fullName: ['', [Validators.required]],
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required]],
      confirm: ['', [this.confirmValidator]],
    });
  }
  ngOnInit(): void {}

  getImg = (): string => {
    return "url('https://www.img.in.th/images/1d2228cb3b466ce2377e4c2b246b33f7.jpg')";
  };

  submitForm = (): void => {
    for (const i in this.registerForm.controls) {
      if (this.registerForm.controls.hasOwnProperty(i)) {
        this.registerForm.controls[i].markAsDirty();
        this.registerForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.registerForm.valid) {
      console.log('loop submitForm');
      this.register();
    }
  };

  register = (): void => {
    console.log('loop');
    const data = {
      Fullname: this.registerForm.controls.fullName.value as string,
      Email: this.registerForm.controls.email.value as string,
      Password: this.registerForm.controls.password.value as string,
    };

    console.log(data);

    this.apicall.register(data).then(
      (res) => {
        console.log(res);

        if (res.message === 'Register Success') {

          this.success('สมัครสมาชิกสำเร็จ')

          void this.router.navigateByUrl('/login')
        }
      },
      (err: HttpErrorResponse) => {
        if (err.status === 400) {
          this.error('ไม่สามารถทำรายการได้<br> เนื่องจากมี Email นี้ในระบบแล้ว');
        } else {
          this.error('ระบบมีปัญหากรุณาลองใหม่อีกครั้ง');
        }
      }
    );
    // if (e === true) {
    //   this.isLoading = true;

    // }
  };

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.registerForm.reset();
    for (const key in this.registerForm.controls) {
      if (this.registerForm.controls.hasOwnProperty(key)) {
        this.registerForm.controls[key].markAsPristine();
        this.registerForm.controls[key].updateValueAndValidity();
      }
    }
  }

  validateConfirmPassword(): void {
    setTimeout(() =>
      this.registerForm.controls.confirm.updateValueAndValidity()
    );
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type

  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true };
    } else if (control.value !== this.registerForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  success = (msg: string): void => {
    const modal = this.modalService.success({
      nzTitle: 'Success',
      nzContent: `${msg}`,
      nzOkText: null,
    });
    setTimeout(() => modal.destroy(), 2000);
  };

  error = (msg: string): void => {
    const modal = this.modalService.error({
      nzTitle: 'Error',
      nzContent: `${msg}`,
    });
    setTimeout(() => modal.destroy(), 5000);
  };
}
