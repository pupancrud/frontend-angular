import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { UserService } from 'src/app/shared/service/user.service';

import { UserModel } from './_model/user.model';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  today = new Date(2021, 0);
  userData: Array<UserModel>;
  isModalUpdate: boolean;
  fullName: string;
  id: number;
  isLoading: boolean;
  name: any;

  constructor(
    private apicall: UserService,
    private notification: NzNotificationService,
    private modal: NzModalService
  ) {
    this.userData = [] as Array<UserModel>;
    this.isModalUpdate = false;
    this.fullName = '';
    this.isLoading = false;
    this.id = 0;
  }

  ngOnInit(): void {
    this.name = localStorage.getItem('name')

    this.getUser();
  }

  getUser = (): void => {
    this.isLoading = true;

    this.apicall.getUser().then((res) => {
      console.log(res);
      this.userData = res.data;
      this.isLoading = false;
    });
  };

  editUser = (data: UserModel): void => {
    this.isModalUpdate = true;

    console.log(data);
    this.id = data.id;
    this.fullName = data.full_name;
  };

  deleteUser = (data: UserModel): void => {
    console.log(data);

    this.modal.confirm({
      nzTitle: 'คุณแน่ใจว่าจะลบข้อมูลผู้ใช้คนนี้?',
      nzContent: '<b></b>',
      nzOnOk: () => {
        this.isLoading = true;

        void this.apicall.deleteUser(data.id).then(
          (res: UserModel) => {
            //data.message, 'response');
            this.isLoading = false;
            // this.getUser(this.pageIndex, this.pageSize);
            this.success('ลบข้อมูลสำเร็จ !');
          },

          (err: any) => {
            this.isLoading = false;

            //'Delete User error ->', err);
            this.error('ไม่สามารถลบผู้ใช้งานนี้ได้ !');
          }
        );
        this.userData = this.userData.filter((d) => d.id !== data.id);

        // var x = data.name;
        // alert('delete:' + '' + x);
        //this.userList);
      },
    });
  };

  cancelEdit = (): void => {
    this.isModalUpdate = false;
    this.fullName = '';
    this.id = 0;
  };

  submitEdit = (): void => {
    this.isModalUpdate = false;
    this.isLoading = true;
    console.log(this.fullName);

    this.apicall.updateUser(this.id, this.fullName).then((res) => {
      // "message": "Update success"

      if (res.message === 'Update success') {
        this.success('แก้ไขข้อมูลสำเร็จ!');
        this.getUser();
        this.isModalUpdate = false;
        this.isLoading = false;

        this.fullName = '';
        this.id = 0;
      } else {
        this.error('ไม่สามารถทำรายการได้กรุณาลองใหม่');
        this.isModalUpdate = false;
        this.isLoading = false;

        this.fullName = '';
        this.id = 0;
      }
    });
  };

  success(msg: string): void {
    this.notification.create('success', msg, '');
  }

  error(msg: string): void {
    this.notification.create('error', msg, '');
  }
}
