import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
// import * as CryptoJS from 'crypto-js';
import { tap } from 'rxjs/operators';
import { RouteConfigLoadEnd } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // url = 'localhost:5000'

  private prefixUri = '/api';
  private uri = (uri: string = ''): string => `${this.prefixUri}${uri}`;

  constructor(private http: HttpClient) {

  }

  getSignInUrl = async (): Promise<string> => {
    // return of(
    //   'https://one.th/api/oauth/getcode?client_id=216&response_type=code&scope=""'
    // ).toPromise();
    return this.http
      .get<any>(this.uri('/v1/users/login'))
      .toPromise()
      .then((r) => r.data as string);
  };

  isAuthorizedAccess(): Promise<boolean> {
    // let isAccess = false;
    // if (localStorage.getItem('web:token') !== null) {
    //   isAccess = true;
    // }
    // return of(isAccess).toPromise();
    return this.http
      .get(this.uri('/v1/token'))
      .toPromise()
      .then(() => true)
      .catch((err) => {
        localStorage.clear();

        return false;
      });
  }

  signOut(): Promise<boolean> {
    return this.http
      .post(this.uri('/v1/signout'), null)
      .pipe(
        tap(() => {
          localStorage.clear();
        })
      )
      .toPromise()
      .then(() => true)
      .catch(() => false);
  }





  isAccessPosition = (positionList: Array<string>): boolean => {
    const via = localStorage.getItem('via') as string;
    const myPosition = localStorage.getItem(`${via}:position`);
    if (positionList) {
      for (const pos of positionList) {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
        if (myPosition === pos) {
          return true;
        }
      }
      return false;
    } else {
      return true;
    }
  };

  getUserDeptUid = (): string => {
    const via = localStorage.getItem('via') as string;
    return localStorage.getItem(`${via}:dept_uid`) as string;
  };

  getUserProfile = (): any => {
    const via = localStorage.getItem('via') as string;
    return JSON.parse(
      localStorage.getItem(`${via}:profile`) as string
    ) as any;
  };

  verifySSOAuthorizeBotCode = (code: string): Promise<any> => {
    const data = {
      auth_code: code
    };
    return this.http
      .post<any>(this.uri(`/v1/signin?ui=0`), data)
      .pipe(
        tap(
          (result) => {
            const authorizedResult = result.data as any;
            const profile = authorizedResult.profile;
            console.log('auth data->', authorizedResult);
            localStorage.setItem('via', 'bot');
            localStorage.setItem('bot:token', authorizedResult.token);
            localStorage.setItem('bot:type', authorizedResult.type);
            localStorage.setItem('bot:name_en', profile.first_name_eng);
            localStorage.setItem('bot:dept_uid', profile.department_uid);
            localStorage.setItem('bot:position', profile.position);
            localStorage.setItem('bot:profile', JSON.stringify(profile));
          },
          () => {
            localStorage.clear();
          }
        )
      )
      .toPromise();
  };

  verifySSOAuthorizedCode = async (code: string): Promise<any> => {
    const data = {
      auth_code: code
    };
    // localStorage.setItem(
    //   'web:token',
    //   'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJjZjAyOTI1Yy0xYWFlLTQxYTQtYTU1ZC02NTc0ZGE4MWQyNGEiLCJpYXQiOjE2MjY5MzIxMjksImlzcyI6IkFzc2V0cyBUcmFja2luZyIsInN1YiI6Ijk4MTI5NTEwNzg0In0.qt7l6Pu6Yl6dRHvHlGtVyu3i01bUIhZiOYNyt91TE7mmNu5ALWijHjpmU0RvmdQ1tfqE7Mbn6Ctr9l_dL7qoPEUxVI-kGvucqqLBXdrIb9zi0BvjgL5akbHOSx1ywNc8zEfcQXzu71DpkKbOU4ZGcJ1Fi1INJ77MtRAocnA40gsHCVwxE1byT49cSvVB8DzEbsqEXuItKfW1_IhkLmy5Y0UVjDA6kFN1sETsaWIkd2w9ayOd32185GnUMLn-z4WhlgHb-SSaR53ZzC3_3INwjXuV8LYKwjSp1uM5wttQAznQVgC5vQkB3OdP0RpWi1uGVnPzvzxY0rVFo0YmknW4ww'
    // );

    // localStorage.setItem('web:position', 'employee');
    // localStorage.setItem('web:name_en', 'aom.washi');
    // localStorage.setItem('web:dept', 'IoT Ecosystem');
    // localStorage.setItem('web:dept_id', '0cae0250-4cbe-11ea-a21b-b38f94c93fd6');

    // return of({ data: 'success' } as any).toPromise();
    return this.http
      .post<any>(this.uri(`/v1/signin?ui=1`), data)
      .pipe(
        tap(
          (result) => {
            const authorizedResult = result.data as any;
            const profile = authorizedResult.profile;
            console.log('auth data->', authorizedResult);
            localStorage.setItem('via', 'web');
            localStorage.setItem('web:token', authorizedResult.token);
            localStorage.setItem('web:type', authorizedResult.type);
            localStorage.setItem('web:name_en', profile.first_name_eng);
            localStorage.setItem('web:dept_uid', profile.department_uid);
            localStorage.setItem('web:position', profile.position);
            localStorage.setItem('web:profile', JSON.stringify(profile));
          },
          () => {
            localStorage.clear();
          }
        )
      )
      .toPromise();
  };
}
