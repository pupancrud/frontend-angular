import { Injectable } from '@angular/core';
import { ResultAPI, Register } from '../model/user.model';
import { Observable, of } from 'rxjs';
// import { MockAssetData } from 'src/app/features/assets/_mock-data/asset-mock-data';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root',
})
export class LoginService {
  uri = 'http://localhost:5000/api/v1';
  constructor(private http: HttpClient) {}

  getProfile = (): Promise<any> => {
    // let options = new RequestOptions({ headers: headers });

    return this.http.get(`api/v1/users/`).toPromise();
  };

  login = (data: any): Promise<any> => {
    var endpoint = {
      Email: data.email,
      Password: data.password,
    };

    return this.http.post<any>(`${this.uri}/users/login`, endpoint).toPromise();
  };

  register = (data: Register): Promise<ResultAPI> => {
    console.log(data);

    return this.http
      .post<ResultAPI>(`${this.uri}/users/register`, data)
      .toPromise();
  };
}
