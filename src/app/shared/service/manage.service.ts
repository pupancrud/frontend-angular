import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
// import { MockAssetData } from 'src/app/features/assets/_mock-data/asset-mock-data';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ManageService {
  constructor(private http: HttpClient) {}

  getApiTest = (): Promise<any> => {

    return this.http
      .get<any>(`https://catfact.ninja/fact`)
      .toPromise();
  };



  getuserProfile = (): Promise<any> => {
    return this.http.get<any>(`/api/v1/bb`).toPromise();
  };


}
