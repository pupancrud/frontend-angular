import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  getUser = (): Promise<any> => {
    return this.http.get(`api/v1/users`).toPromise();
  };

  updateUser = (id: number, data: any): Promise<any> => {
    var fullname = data;

    return this.http.put(`api/v1/users/${id}`, { fullname }).toPromise();
  };

  deleteUser = (id: number): Promise<any> => {
    return this.http.delete(`api/v1/users/${id}`).toPromise();
  };
}
